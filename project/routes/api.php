<?php

use App\Http\Controllers\api\ResiduoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/upload-content',[ResiduoController::class,'uploadContent']);
Route::get('/export-content',[ResiduoController::class,'exportData']);
Route::get('/list-content',[ResiduoController::class,'index']);
Route::put('/edit', [ResiduoController::class,'update']);
Route::delete('/delete', [ResiduoController::class,'destroy']);
Route::get('/show/{id}', [ResiduoController::class,'show']);
Route::post('/create', [ResiduoController::class,'store']);
