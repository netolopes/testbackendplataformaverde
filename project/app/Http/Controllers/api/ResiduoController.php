<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Resources\ResiduoResource;
use App\Models\Residuo;
use App\Services\Contracts\IResiduoService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use App\Jobs\ResiduoProcess;

class ResiduoController extends  Controller
{

    protected $residuoService;

    public function __construct
    (
        IResiduoService $residuoService
    )
    {
        $this->residuoService = $residuoService;
    }


    public function index(Request $request)
    {

        if(request()->ajax()) {
            return datatables()->of(Residuo::select('*'))
            ->addColumn('action', 'front.residuo-action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }

        try{

            $data = $this->residuoService->list();
            return ResiduoResource::collection($data);

        } catch (\Exception $e) {
            return response()->json(
                [
                    'success' => false,
                    'error' => $e->getMessage()
                ],
                403
            );
        }


    }

    public function uploadContent(Request $request)
    {




        $this->validate($request, [
            'uploaded_file' => 'required|file|mimes:xls,xlsx'
        ]);
        $the_file = $request->file('uploaded_file');

        if ($the_file) {

            $filename = $the_file->getClientOriginalName();
            $extension = $the_file->getClientOriginalExtension();
            $fileSize = $the_file->getSize();
            $this->checkUploadedFileProperties($extension, $fileSize);

            try {
                $spreadsheet = IOFactory::load($the_file->getRealPath());
                // $inputFileName = public_path('public/docs/Filename.xlsx');
                // $location = 'uploads';
                // $the_file->move($location, $inputFileName);
                $sheet        = $spreadsheet->getActiveSheet();
                $row_limit    = $sheet->getHighestDataRow();
                $column_limit = $sheet->getHighestDataColumn();
                $row_range    = range(6, $row_limit);
                $column_range = range('H', $column_limit);

                $data = $this->residuoService->uploadContent($row_range,$sheet);
                return $data;

            } catch (Exception $e) {
                throw $e;

            }
            return response()->json([
                'message' => "startcount records successfully uploaded"
            ]);
        } else {
            throw new \Exception('No file was uploaded', Response::HTTP_BAD_REQUEST);
        }

    }

    public function checkUploadedFileProperties($extension, $fileSize)
    {
        $valid_extension = array("csv", "xlsx", "xlx"); //Only want csv and excel files
        $maxFileSize = 2097152; // Uploaded file size limit is 2mb
        if (in_array(strtolower($extension), $valid_extension)) {
            if ($fileSize <= $maxFileSize) {
            } else {
                throw new \Exception('Error, no file was uploaded', Response::HTTP_REQUEST_ENTITY_TOO_LARGE); //413 error
            }
        } else {
            throw new \Exception('Invalid file extension', Response::HTTP_UNSUPPORTED_MEDIA_TYPE); //415 error
        }
    }


    public function exportExcel($customer_data){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '4000M');
        try {
            $spreadSheet = new Spreadsheet();

            $spreadSheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(20);
            $spreadSheet->getActiveSheet()->fromArray($customer_data,  NULL, 'B5' );
            $spreadSheet->getActiveSheet()->getStyle('B5:I5')->getFont()->setBold(true);
            $spreadSheet->getActiveSheet()->getStyle('B5:I5')->getFont()->setBold(true);
            $spreadSheet->getActiveSheet()->getStyle('B5:I5')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

            $Excel_writer = new Xls($spreadSheet);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="planilha_residuos.xls"');
            header('Cache-Control: max-age=0');
            ob_end_clean();
            $Excel_writer->save('php://output');
            exit();
        } catch (Exception $e) {
            return;
        }
    }


    public function exportData()
    {
        $data = $this->residuoService->list();
        $data_array [] = array("Nome Comum do Resíduo","Tipo de Resíduo","Categoria","Tecnologia de Tratamento","Classe","Unidade de Medida","Peso");
        foreach($data as $data_item)
        {
            $data_array[] = array(
                'Nome Comum do Resíduo' => $data_item->nome,
                'Tipo de Resíduo' => $data_item->tipo,
                'Categoria' => $data_item->categoria,
                'Tecnologia de Tratamento' => $data_item->tecnologia,
                'Classe' => $data_item->classe,
                'Unidade de Medida' =>$data_item->unidade,
                'Peso' =>$data_item->peso
            );
        }

          $this->exportExcel($data_array);
       //  ResiduoProcess::dispatch($data_array);

    }

    public function update(Request $request)
    {

        try{

            $data = (object)$request->only([
                'id',
                'nome',
                'tipo',
                'categoria' ,
                'tecnologia',
                'classe',
                'unidade',
                'peso'
            ]);

            $data = $this->residuoService->edit($data->id,
                                                $data->nome,
                                                $data->tipo,
                                                $data->categoria,
                                                $data->tecnologia,
                                                $data->classe,
                                                $data->unidade,
                                                $data->peso
                                            );
            return $data;

        } catch (\Exception $e) {
            return response()->json(
                [
                    'success' => false,
                    'error' => $e->getMessage()
                ],
                403
            );
        }

    }

    public function destroy(Request $request)
    {
        try{

            $data = (object)$request->only([
                'id'
            ]);

            $data = $this->residuoService->delete($data->id);
            return $data;

        } catch (\Exception $e) {
            return response()->json(
                [
                    'success' => false,
                    'error' => $e->getMessage()
                ],
                403
            );
        }
    }

    public function show($id)
    {
        try{

            $data = $this->residuoService->listBy($id);
            return $data;

        } catch (\Exception $e) {
            return response()->json(
                [
                    'success' => false,
                    'error' => $e->getMessage()
                ],
                403
            );
        }
    }


}
