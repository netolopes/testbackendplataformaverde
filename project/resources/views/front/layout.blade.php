<!DOCTYPE html>
<html>
<head>
    <title>Reports</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
    <script src="{{ asset('public/resources/js/todo.js') }}" defer></script>
</head>
<body>

<div class="container">
    @yield('content')
</div>

</body>
</html>
