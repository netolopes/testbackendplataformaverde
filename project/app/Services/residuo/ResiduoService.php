<?php
namespace App\Services\residuo;

use App\Repositories\Contracts\IResiduoRepository;
use App\Services\Contracts\IResiduoService;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Style\Alignment;


class ResiduoService implements IResiduoService
{

    protected $residuoRepository;

    public function __construct
    (
        IResiduoRepository $residuoRepository
    ) {
       $this->residuoRepository = $residuoRepository;
    }

    public function list()
    {
        $data = $this->residuoRepository->list();
        return $data;
    }

    public function uploadContent($row_range,$sheet)
    {
        $data = $this->residuoRepository->uploadContent($row_range,$sheet);
        return $data;
    }


    public function edit(int $id,
                        string $nome,
                        string $tipo,
                        string $categoria,
                        string $tecnologia,
                        string $classe,
                        string $unidade,
                        string $peso)
    {
        $data = $this->residuoRepository->edit( $id,
                                                $nome,
                                                $tipo,
                                                $categoria,
                                                $tecnologia,
                                                $classe,
                                                $unidade,
                                                $peso
                                              );
        return $data;
    }

    public function delete(int $id)
    {
        $data = $this->residuoRepository->delete($id);
        return $data;
    }

    public function listBy(int $id)
    {
        $data = $this->residuoRepository->listBy($id);
        return $data;
    }


}
