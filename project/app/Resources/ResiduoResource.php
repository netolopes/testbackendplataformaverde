<?php
namespace App\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class ResiduoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nome' => $this->nome,
            'tipo' => $this->tipo,
            'categoria' => $this->categoria,
            'tecnologia' => $this->tecnologia,
            'classe' => $this->classe,
            'unidade' => $this->unidade,
            'peso' => $this->peso
        ];
    }
}
