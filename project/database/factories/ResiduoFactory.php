<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ResiduoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nome' => $this->faker->name(),
            'tipo' => $this->faker->company(),
            'categoria' => $this->faker->title(),
            'tecnologia' => $this->faker->title(),
            'classe' => $this->faker->randomElement(['||-A','||-B','||-C']),
            'unidade' => $this->faker->randomElement(['un','ton','kg']),
            'peso' => $this->faker->randomElement(['10','30','80'])
        ];
    }


}
