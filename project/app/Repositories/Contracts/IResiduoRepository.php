<?php

namespace App\Repositories\Contracts;

interface IResiduoRepository
{
    public function list();
    public function uploadContent($row_range,$sheet);
    public function edit(int $id,
                        string $nome,
                        string $tipo,
                        string $categoria,
                        string $tecnologia,
                        string $classe,
                        string $unidade,
                        string $peso
                        );
    public function delete(int $id);
    public function listBy(int $id);

}
