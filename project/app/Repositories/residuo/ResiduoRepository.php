<?php
namespace App\Repositories\residuo;

use App\Models\Residuo;
use App\Repositories\Contracts\IResiduoRepository;

class ResiduoRepository  implements IResiduoRepository
{

    protected $residuo;

    public function __construct
    (
       Residuo $residuo
    ) {
       $this->residuo = $residuo;
    }

    public function list()
    {
        $data = Residuo::orderBy('id', 'ASC')->get();
        return $data;
    }

    public function uploadContent($row_range,$sheet)
    {

        $startcount = 0;
        foreach ($row_range as $row) {
            Residuo::create([
                'nome' => $sheet->getCell('B' . $row)->getValue(),
                'tipo' => $sheet->getCell('C' . $row)->getValue(),
                'categoria' => $sheet->getCell('D' . $row)->getValue(),
                'tecnologia' => $sheet->getCell('E' . $row)->getValue(),
                'classe' => $sheet->getCell('F' . $row)->getValue(),
                'unidade' => $sheet->getCell('G' . $row)->getValue(),
                'peso'  => $sheet->getCell('H' . $row)->getValue(),
            ]);
            $startcount++;
        }
        return $startcount;
    }

    public function edit(int $id,
                        string $nome,
                        string $tipo,
                        string $categoria,
                        string $tecnologia,
                        string $classe,
                        string $unidade,
                        string $peso
                        )
    {

           $obj = $this->residuo::findOrFail($id);
           $obj->nome = $nome;
           $obj->tipo = $tipo;
           $obj->categoria = $categoria;
           $obj->tecnologia =  $tecnologia;
           $obj->classe = $classe;
           $obj->unidade = $unidade;
           $obj->peso = $peso;

           if( $obj->save() ){
            return  $obj;
          }
    }

    public function delete(int $id){

        $obj =  $this->residuo::findOrFail($id);

          if( $obj->delete() ){
           return  $obj;
         }
   }

   public function listBy(int $id)
    {
        $data =  $this->residuo::findOrFail($id);
        return $data;
    }



}
