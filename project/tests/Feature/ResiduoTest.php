<?php

namespace Tests\Feature\Api;

use App\Models\Residuo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ResiduoTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_get_all_residuos()
    {
        $response = $this->getJson('/api/list-content');

        $response->assertStatus(200);
    }


    public function test_get_count_residuos()
    {
        Residuo::factory()->count(10)->create();

        $response = $this->getJson('/api/list-content');

        $response->assertJsonCount(10, 'data');
        $response->assertStatus(200);
    }

    public function test_notfound_residuos()
    {
        $response = $this->getJson('/api/list-content/fake_value');

        $response->assertStatus(404);
    }

    public function test_get_residuo()
    {
        $res = Residuo::factory()->create();

        $response = $this->getJson("/api/show/{$res->id}");

        $response->assertStatus(200);
    }

}
