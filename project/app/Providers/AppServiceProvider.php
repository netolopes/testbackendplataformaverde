<?php

namespace App\Providers;

use App\Repositories\Contracts\IResiduoRepository;
use App\Repositories\residuo\ResiduoRepository;
use App\Services\Contracts\IResiduoService;
use App\Services\residuo\ResiduoService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
         //services
         $this->app->bind(IResiduoService::class,ResiduoService::class);

         //repositories
         $this->app->bind(IResiduoRepository::class, ResiduoRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
