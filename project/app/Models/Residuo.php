<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Residuo extends Model
{
    use HasFactory;

    protected $table = 'residuos';

    protected $fillable = [
        'nome',
        'tipo',
        'categoria',
        'tecnologia',
        'classe',
        'unidade',
        'peso'
    ];
}
