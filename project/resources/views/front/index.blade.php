<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <title>Laravel</title>
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
      <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
      <link  href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
      <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
   </head>
   <body>
      <div class="container mt-4">
         <div class="row">
            <div class="col-lg-4 margin-tb">
                <div class="container">
                    <form method="post" action="" enctype="multipart/form-data" id="myform">

                        <div >
                            <input type="file" id="uploaded_file" name="uploaded_file" />
                            <input type="button" class="button" value="Importar" id="but_upload">
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-8 margin-tb">
                <div style="text-align: right">
                   <a href="{{url("api/export-content")}}" class="btn btn-primary" >Process/Export Excel</a>
                </div>
             </div>
         </div>
         @if ($message = Session::get('success'))
         <div class="alert alert-success">
            <p>{{ $message }}</p>
         </div>
         @endif
         <div class="card-body">
            <table class="table table-bordered" id="ajax-crud-datatable">
               <thead>
                  <tr>
                     <th>Id</th>
                     <th>Nome</th>
                     <th>Tipo</th>
                     <th>Categoria</th>
                     <th>Tecnologia</th>
                     <th>Calsse</th>
                     <th>Unidade</th>
                     <th>Peso</th>
                     <th>Action</th>
                  </tr>
               </thead>
            </table>
         </div>
      </div>
      <!-- boostrap company model -->
      <div class="modal fade" id="company-modal" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title" id="CompanyModal"></h4>
               </div>
               <div class="modal-body">
                  <form action="javascript:void(0)" id="CompanyForm" name="CompanyForm" class="form-horizontal" method="POST" enctype="multipart/form-data">
                     <input type="hidden" name="id" id="id">
                     <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Nome</label>
                        <div class="col-sm-12">
                           <input type="text" class="form-control" id="nome" name="nome" placeholder="Enter Name" maxlength="50" required="">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Tipo</label>
                        <div class="col-sm-12">
                           <input type="text" class="form-control" id="tipo" name="tipo" placeholder="Enter Tipo" maxlength="50" required="">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label">Categoria</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="categoria" name="categoria" placeholder="Enter Categoria" maxlength="50" required="">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label">Tecnologia</label>
                        <div class="col-sm-12">
                           <input type="text" class="form-control" id="tecnologia" name="tecnologia" placeholder="Enter Tecnologia" required="">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label">Classe</label>
                        <div class="col-sm-12">
                           <input type="text" class="form-control" id="classe" name="classe" placeholder="Enter classe" required="">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label">Unidade</label>
                        <div class="col-sm-12">
                           <input type="text" class="form-control" id="unidade" name="unidade" placeholder="Enter unidade" required="">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label">Peso</label>
                        <div class="col-sm-12">
                           <input type="text" class="form-control" id="peso" name="peso" placeholder="Enter peso" required="">
                        </div>
                     </div>
                     <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary" id="btn-save">Save changes
                        </button>
                     </div>
                  </form>
               </div>
               <div class="modal-footer"></div>
            </div>
         </div>
      </div>
      <!-- end bootstrap model -->
   </body>
   <script type="text/javascript">
    $(document).ready( function () {
      $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $('#ajax-crud-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('api/list-content') }}",
        columns: [
        { data: 'id', name: 'id' },
        { data: 'nome', name: 'nome' },
        { data: 'tipo', name: 'tipo' },
        { data: 'categoria', name: 'categoria' },
        { data: 'tecnologia', name: 'tecnologia' },
        { data: 'classe', name: 'classe' },
        { data: 'unidade', name: 'unidade' },
        { data: 'peso', name: 'peso' },
        {data: 'action', name: 'action', orderable: false},
        ],
        order: [[0, 'desc']]
      });
    });
    function editFunc(id){
      $.ajax({
        type:"get",
        url: "{{ url('api/show') }}"+"/"+id,
        dataType: 'json',
      success: function(res){
        $('#CompanyModal').html("Edit Company");
        $('#company-modal').modal('show');
        $('#id').val(res.id);
        $('#nome').val(res.nome);
        $('#tipo').val(res.tipo);
        $('#categoria').val(res.categoria);
        $('#tecnologia').val(res.tecnologia);
        $('#classe').val(res.classe);
        $('#unidade').val(res.unidade);
        $('#peso').val(res.peso);
      }
      });
    }


    $("#but_upload").click(function(){

    var fd = new FormData();
    var files = $('#uploaded_file')[0].files;

    // Check file selected or not
    if(files.length > 0 ){
            fd.append('uploaded_file',files[0]);

            $.ajax({
                url: 'api/upload-content',
                type: 'post',
                data: fd,
              contentType: false,
              processData: false,
                success: function(response){
                    if(response != 0){
                        alert('file uploaded success!');
                        var oTable = $('#ajax-crud-datatable').dataTable();
                        oTable.fnDraw(false);
                    }else{
                        alert('file not uploaded');
                    }
                },
            });
        }else{
            alert("Please select a file.");
        }
    });

    function exportExcel(){

      $.ajax({
        type:"get",
        url: "{{ url('api/export-content') }}",
        dataType: 'json',
      success: function(res){
        var oTable = $('#ajax-crud-datatable').dataTable();
        oTable.fnDraw(false);
      }
      });

    }

    function deleteFunc(id){
      if (confirm("Delete Record "+ id +"?") == true) {
      var id = id;
      // ajax
      $.ajax({
        type:"DELETE",
        url: "{{ url('api/delete') }}",
        data: { id: id },
        dataType: 'json',
      success: function(res){
        var oTable = $('#ajax-crud-datatable').dataTable();
        oTable.fnDraw(false);
      }
      });
      }
    }
    $('#CompanyForm').submit(function(e) {
      e.preventDefault();
      var formData = new FormData(this);
      var check_id = document.forms['CompanyForm'].elements['id'].value;
      var result = "";

      let  nome           = document.forms['CompanyForm'].elements['nome'].value;
      let  tipo           = document.forms['CompanyForm'].elements['tipo'].value;
      let  categoria      = document.forms['CompanyForm'].elements['categoria'].value;
      let  tecnologia     = document.forms['CompanyForm'].elements['tecnologia'].value;
      let  classe         = document.forms['CompanyForm'].elements['classe'].value;
      let  unidade        = document.forms['CompanyForm'].elements['unidade'].value;
      let  peso           = document.forms['CompanyForm'].elements['peso'].value;

      var data = {
         "id":check_id,
         "nome":nome,
         "tipo":tipo,
         "categoria":categoria,
         "tecnologia":tecnologia,
         "classe":classe,
         "unidade":unidade,
         "peso":peso
         }
        $.ajax({
                type:'PUT',
                url: "{{ url('api/edit')}}",
                data: data,
            success: (data) => {
                $("#company-modal").modal('hide');
                var oTable = $('#ajax-crud-datatable').dataTable();
                oTable.fnDraw(false);
                $("#btn-save").html('Submit');
                $("#btn-save"). attr("disabled", false);
            },
            error: function(jqXHR, textStatus, errorThrown){
            alert(textStatus + ": " + jqXHR.status + " " + errorThrown);
        }
        });


    });
   </script>
</html>


